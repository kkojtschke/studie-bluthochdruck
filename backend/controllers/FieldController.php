<?php

namespace backend\controllers;

use Yii;
use common\models\Field;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * FieldController implements the CRUD actions for Field model.
 */
class FieldController extends Controller{
    public function behaviors(){
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }

    /**
     * Creates a new Field model.
     * @param integer $questionnaire_id
     * @param integer $copy_from_field_id
     * @return mixed
     */
    public function actionCreate($questionnaire_id, $copy_from_field_id = null){
        $model = new Field();

        // Duplikat eines Feldes erzeugen
        if($copy_from_field_id != null){
            $fieldToClone = Field::findOne($copy_from_field_id);
            if($fieldToClone){
                $model->attributes = $fieldToClone->attributes;
            }
        }

        $model->questionnaire_id = $questionnaire_id;

        // Feldnamen generieren
        $fieldNamePrefix = 'Feld';
        $fieldNameSuffix = 1;
        while(Field::findOne(
                ['name' => $fieldNamePrefix . $fieldNameSuffix, 'questionnaire_id' => $questionnaire_id]
            ) != null){
            ++$fieldNameSuffix;
        }
        $model->name = $fieldNamePrefix . $fieldNameSuffix;

        $model->sorting = Field::find()->max('sorting') + 1;

        $model->save();
        return $this->redirect(['questionnaire/update', 'id' => $questionnaire_id]);
    }

    /**
     * Ändert ein Feld. Die zu änderne Eigenschaft wird per AJAX gesendet.
     * @return mixed
     */
    public function actionUpdate(){
        // wenn per AJAX Daten für die Felder gesendet wurden
        if(Yii::$app->request->post('hasEditable')){
            // zu änderndes Feld instanziieren
            $fieldId = Yii::$app->request->post('editableKey');
            $field = Field::findOne($fieldId);

            // holt den ersten Eintrag der geposteten Feld-Daten
            // - $posted is the posted data for Field without any indexes
            // - $post is the converted array for single model validation
            $posted = current($_POST['Field']);
            $post = ['Field' => $posted];

            // Standardantwort
            // durch 'output' => current($posted) wird der eingegebene Wert bestätigt
            $output = current($posted);
            $message = '';
            $reload = false;

            // Bei Änderung des Feldtyps die typspezifischen Einstellungen zurücksetzen
            // und Neuladen der Seite erzwingen
            if(isset($posted['type']) && $posted['type'] != $field->type){
                $field->settings = null;
                $reload = true;
            }

            // Bei Änderung der Feld-Einstellungen Neuladen der Seite erzwingen
            if(isset($posted['settings'])){
                $reload = true;
            }

            /* Speichern */
            if($field->load($post)){
                $field->save();
            }

            // Antwort senden
            echo Json::encode(['output' => $output, 'message' => $message, 'reload' => $reload]);
            return;
        }
    }

    /**
     * Deletes an existing Field model.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id){
        $model = $this->findModel($id);
        $model->delete();

        Field::updateAll(['condition' => null], '`condition` = ' . $id);

        return $this->redirect(['questionnaire/update', 'id' => $model->questionnaire_id]);
    }

    /**
     * Verschiebt ein Feld um eine Position nach oben.
     * @param string $id
     * @return mixed
     */
    public function actionMoveUp($id){
        $model = $this->findModel($id);

        $fields = Field::find()->where(['questionnaire_id' => $model->questionnaire_id])->orderBy('sorting')->all();
        $indexOfPreviousField = null;
        foreach($fields as $index => $field){
            if($field->id == $id && $index > 0){
                $previousField = $fields[$index - 1];
                $sortingOfPreviousField = $previousField->sorting;
                $previousField->sorting = $model->sorting;
                $model->sorting = $sortingOfPreviousField;
                $model->save();
                $previousField->save();
                break;
            }
        }

        return $this->redirect(['questionnaire/update', 'id' => $model->questionnaire_id]);
    }

    /**
     * Verschiebt ein Feld an die oberste Position.
     * @param string $id
     * @return mixed
     */
    public function actionMoveTop($id){
        $model = $this->findModel($id);

        $firstField = Field::find()->where(['questionnaire_id' => $model->questionnaire_id])->orderBy('sorting')->limit(1)
            ->one();

        $model->sorting = $firstField->sorting - 1;
        $model->save();

        return $this->redirect(['questionnaire/update', 'id' => $model->questionnaire_id]);
    }

    /**
     * Verschiebt ein Feld um eine Position nach unten.
     * @param string $id
     * @return mixed
     */
    public function actionMoveDown($id){
        $model = $this->findModel($id);

        $fields = Field::find()->where(['questionnaire_id' => $model->questionnaire_id])->orderBy('sorting')->all();
        $maxIndex = count($fields) - 1;
        $indexOfPreviousField = null;
        foreach($fields as $index => $field){
            if($field->id == $id && $index < $maxIndex){
                $nextField = $fields[$index + 1];
                $sortingOfNextField = $nextField->sorting;
                $nextField->sorting = $model->sorting;
                $model->sorting = $sortingOfNextField;
                $model->save();
                $nextField->save();
                break;
            }
        }

        return $this->redirect(['questionnaire/update', 'id' => $model->questionnaire_id]);
    }

    /**
     * Verschiebt ein Feld an die letzte Position.
     * @param string $id
     * @return mixed
     */
    public function actionMoveBottom($id){
        $model = $this->findModel($id);

        $fields = Field::find()->where(['questionnaire_id' => $model->questionnaire_id])->orderBy('sorting')
            ->all();
        $lastField = $fields[count($fields) - 1];

        $model->sorting = $lastField->sorting + 1;
        $model->save();

        return $this->redirect(['questionnaire/update', 'id' => $model->questionnaire_id]);
    }

    /**
     * Finds the Field model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Field the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id){
        if(($model = Field::findOne($id)) !== null){
            return $model;
        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
