<?php

namespace backend\controllers;

use common\models\Proband;
use common\models\Questionnaire;
use common\models\User;
use common\models\Value;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * ProbandController implements the CRUD actions for Proband model.
 */
class ProbandController extends Controller{
    public function behaviors(){
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Proband models.
     * @return mixed
     */
    public function actionIndex(){
        $restrictions = [];
        if(yii::$app->user->getIdentity()->role == User::ROLE_BIOSENS){
            $restrictions['biosens_id'] = [yii::$app->user->id];
        }

        $dataProvider = new ActiveDataProvider([
            'query' => Proband::find()->where($restrictions),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Proband model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id){
        $questionnaires = Questionnaire::findAll(['group' => Questionnaire::GROUP_REGISTRIERUNG]);

        return $this->render('view', [
            'proband' => $this->findModel($id),
            'questionnaires' => $questionnaires
        ]);
    }

    /**
     * Zeigt die Bögen zur Ersterhebung an.
     * @param integer $id
     * @return mixed
     */
    public function actionErsterhebung($id){
        $questionnaires = Questionnaire::findAll(['group' => Questionnaire::GROUP_ERSTERHEBUNG]);

        return $this->render('ersterhebung', [
            'proband' => $this->findModel($id),
            'questionnaires' => $questionnaires
        ]);
    }

    /**
     * Zeigt die Bögen zur Nacherhebung an.
     * @param integer $id
     * @param bool $studie_abgebrochen
     * @return mixed
     */
    public function actionNacherhebung($id, $studie_abgebrochen = null){
        $conditions = [
            'group' => Questionnaire::GROUP_NACHERHEBUNG
        ];
        if($studie_abgebrochen == true){
            $conditions['hide_when_cancelled'] = 0;
        }
        $questionnaires = Questionnaire::find()->where($conditions)->all();

        return $this->render('nacherhebung', [
            'proband' => $this->findModel($id),
            'studie_abgebrochen' => $studie_abgebrochen,
            'questionnaires' => $questionnaires
        ]);
    }

    /**
     * Creates a new Proband model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate(){
        if(time() > strtotime('2016-02-29')){
            return $this->render('noRegistrationAllowed');
        }

        $proband = new Proband();
        $proband->created_at = time();
        $proband->updated_at = time();

        if($proband->load(Yii::$app->request->post())){
            if($proband->validate()){
                $proband->biosens_id = yii::$app->user->id;
                $proband->save(false);
                return $this->redirect(['view', 'id' => $proband->id]);
            }else{
                var_dump($proband->getErrors());
                die();
            }
        }else{
            return $this->render('create', [
                'proband' => $proband,
            ]);
        }
    }

    /**
     * Updates an existing Proband model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id){
        $proband = $this->findModel($id);
        $proband->updated_at = time();

        if($proband->load(Yii::$app->request->post())){
            if($proband->validate()){
                $proband->save(false);
                return $this->redirect(['view', 'id' => $proband->id]);
            }else{
                var_dump($proband->getErrors());
                die();
            }
        }else{
            return $this->render('update', [
                'proband' => $proband,
            ]);
        }
    }

    /**
     * Deletes an existing Proband model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id){
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Zeigt die hochgeladene Einverständniserklärung an.
     * @param integer $id
     * @return mixed
     */
    public function actionEinverstaendniserklaerung($id){
        $upload_einverstaendniserklaerung = $this->findModel($id)->upload_einverstaendniserklaerung;
        if($upload_einverstaendniserklaerung){
            header('Content-type: application/pdf');
            echo $upload_einverstaendniserklaerung;
        }
    }

    /**
     * Zeigt den hochgeladenen Eignungsbogen an.
     * @param integer $id
     * @return mixed
     */
    public function actionEignungsbogen($id){
        $upload_eignungsbogen = $this->findModel($id)->upload_eignungsbogen;
        if($upload_eignungsbogen){
            header('Content-type: application/pdf');
            echo $upload_eignungsbogen;
        }
    }

    /**
     * Zeigt eine hochgeladene Datei an.
     * @param integer $proband_id
     * @param integer $field_id
     * @return mixed
     */
    public function actionViewFile($proband_id, $field_id){
        $value = Value::findOne(['proband_id' => $proband_id, 'field_id' => $field_id]);
        if($value && $value->content){
            $file = $value->content;
            $additionalData = Json::decode($value->additional_data);
            $fileName = $additionalData['fileName'];
            $mimeType = $additionalData['mimeType'];
            header('Content-type: ' . $mimeType);
            header('Content-Disposition: filename="' . $fileName . '"');
            echo $file;
        }
    }

    /**
     * Finds the Proband model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Proband the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id){
        if(($proband = Proband::findOne($id)) !== null){
            return $proband;
        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * is proband admitted action
     */
    public function actionAdmit($id) {
        $model = $this->findModel($id);
        $model->admitted = 1;
        $model->save();
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * is proband denied
     */
    public function actionDeny($id) {
        $model = $this->findModel($id);
        $model->admitted = 0;
        $model->save();
        return $this->redirect(Yii::$app->request->referrer);
    }
}
