<?php

namespace backend\controllers;

use common\models\Proband;
use Yii;
use common\models\Questionnaire;
use common\models\Field;
use common\models\Value;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * QuestionnaireController implements the CRUD actions for Questionnaire model.
 */
class QuestionnaireController extends Controller{
    public function behaviors(){
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Creates a new Questionnaire model.
     * @param integer $survey_id
     * @param integer $copy_from_questionnaire_id
     * @return mixed
     */
    public function actionCreate($survey_id, $copy_from_questionnaire_id = null){
        $model = new Questionnaire();
        $model->hide_when_cancelled = 0;

        // Duplikat eines Fragebogens erzeugen
        if($copy_from_questionnaire_id != null){
            $questionnaireToClone = Questionnaire::findOne($copy_from_questionnaire_id);
            if($questionnaireToClone){
                $model->attributes = $questionnaireToClone->attributes;
            }
        }

        $model->survey_id = $survey_id;

        if($model->load(Yii::$app->request->post()) && $model->save()){
            if($copy_from_questionnaire_id != null){
                // Felder des Fragebogens duplizieren
                foreach($questionnaireToClone->fields as $fieldToClone){
                    $newField = new Field();
                    $newField->attributes = $fieldToClone->attributes;
                    $newField->questionnaire_id = $model->id;
                    $newField->save();
                }
            }

            return $this->redirect(
                [
                    'update', 'id' => $model->id,
                    'fields' => new ActiveDataProvider([
                        'query' => Field::find()->where(['questionnaire_id' => $model->id])->orderBy('sorting')
                            ->indexBy('id'),
                        'sort' => false
                    ])
                ]
            );
        }else{
            return $this->render('create', [
                'questionnaire' => $model,

            ]);
        }
    }

    /**
     * Updates an existing Questionnaire model.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id){
        $model = $this->findModel($id);

        if($model->load(Yii::$app->request->post())){
            $model->save();
        }

        return $this->render('update', [
            'questionnaire' => $model,
            'fields' => new ActiveDataProvider([
                'query' => Field::find()->where(['questionnaire_id' => $model->id])->orderBy('sorting')->indexBy('id'),
                'sort' => false
            ])
        ]);
    }

    /**
     * Deletes an existing Questionnaire model.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id){
        $model = $this->findModel($id);
        // alle Felder des Fragebogens löschen
        foreach($model->fields as $field){
            $field->delete();
        }
        $model->delete();

        return $this->redirect(['survey/update', 'id' => $model->survey_id]);
    }

    /**
     * Einen Fragebogen ausfüllen.
     * @param integer $id
     * @param integer $proband_id
     * @return mixed
     */
    public function actionFill($id, $proband_id){
        $model = $this->findModel($id);
        $values = [];
        foreach($model->fields as $field){
            $value = Value::findOne(['field_id' => $field->id, 'proband_id' => $proband_id]);
            if(!$value){
                $value = new Value();
                $value->field_id = $field->id;
                $value->proband_id = $proband_id;
                $value->save();
            }
            // die Values werden mit der Sortierungsposition des dazugehörigen Feldes indexiert,
            // damit sie in der richtigen Reihenfolge gerendert werden
            $values[$field->sorting] = $value;
        }
        // Die Values nach der Feld-Position sortieren
        ksort($values);

        if(Value::loadMultiple($values, Yii::$app->request->post()) && Value::validateMultiple($values)){
            foreach($values as $value){
                $value->save(false);
            }

            // Fragebogen als ausgefüllt kennzeichnen
            $proband = Proband::findOne($proband_id);
            $filled_questionnaires = Json::decode($proband->filled_questionnaires);
            if(!in_array($model->id, $filled_questionnaires)) $filled_questionnaires[] = $model->id;
            $proband->filled_questionnaires = Json::encode($filled_questionnaires);
            $proband->save();
        }

        return $this->render('fill', [
            'questionnaire' => $model,
            'proband' => Proband::findOne($proband_id),
            'values' => $values
        ]);
    }

    /**
     * Exportiert eine Gruppe von Fragebögen als Excel-Datei
     * @param $group
     * @return mixed
     */
    public function actionExport($group){
        $questionnaires = Questionnaire::findAll(['group' => $group]);

        // Tabellenblätter
        $sheets = [];
        // Tabellenspalten
        $columns = [];

        foreach($questionnaires as $questionnaire){
            $sheetName = $questionnaire->export_name;

            $values = (new Query())
                ->select('v.proband_id as proband_id, v.content as `content`, f.name as fieldname,'
                    . 'f.type as fieldtype, u.last_name, u.first_name, f.id as fieldId')
                ->from('value v')
                ->join('JOIN', 'field f', 'f.id = v.field_id')
                ->join('JOIN', 'questionnaire q', 'q.id = f.questionnaire_id')
                ->join('JOIN', 'proband p', 'p.id = v.proband_id')
                ->join('JOIN', 'user u', 'u.id = p.biosens_id')
                ->groupBy('v.id')
                ->where(['q.id' => $questionnaire->id, 'f.exportable' => Field::EXPORTABLE_YES])
                ->orderBy('f.sorting')->all();

            // Tabellenzeilen
            $records = [];

            // Spaltendefinitionen für Probandenkennung und Biosens einfügen
            $columns[$sheetName] = [
                'proband_id' => [
                    'header' => 'Proband',
                    'value' => function ($record){
                        return $record['proband_id'];
                    }
                ],
                'biosens' => [
                    'header' => 'Biosens',
                    'value' => function ($record){
                        return $record['biosens'];
                    }
                ]
            ];

            foreach($values as $value){
                $probandId = $value['proband_id'];
                $fieldType = $value['fieldtype'];
                $valueContent = $value['content'];
                $biosens = $value['last_name'] . ', ' . $value['first_name'];

                // Werte für Probandenkennung und Biosens einfügen
                if(!isset($records[$probandId])){
                    $records[$probandId] = [
                        'proband_id' => $probandId,
                        'biosens' => $biosens
                    ];
                }


                // wenn es sich um ein Tabellen-Feld handelt, dann die Werte der Tabellenzellen einfügen
                if($fieldType == Field::TYPE_TABLE || $fieldType == Field::TYPE_BLOOD_PRESSURE_TABLE){
                    $fieldId = $value['fieldId'];
                    // Spaltennamen
                    $columnNames = [];
                    if($fieldType == Field::TYPE_TABLE){
                        $fieldSettings = JSON::decode(Field::findOne($fieldId)->settings);
                        $tableColumns = isset($fieldSettings['columns']) ? $fieldSettings['columns'] : [];

                        foreach($tableColumns as $column){
                            $columnNames[] = $column[1];
                        }
                    }else{
                        $columnNames = ['Tag-%N-Morgens', 'Tag-%N-Mittags', 'Tag-%N-Abends'];
                    }

                    // Feld-Wert in den entsprechenden Datensatz einfügen

                    // Zeilen der Tabelle
                    $rows = JSON::decode($valueContent);
                    // für jede Tabellenzeile werden hier die Spaltennamen eingefügt
                    $fieldNames = [];
                    if(!empty($rows)){
                        foreach($rows as $rowNumber => $row){
                            $fieldNames[$rowNumber] = [];
                            // Spaltenwerte innerhalb der Zeile
                            foreach($row as $n => $colValue){
                                // Platzhalter ersetzen
                                $fieldName = str_replace('%N', $rowNumber + 1, $columnNames[$n]);
                                if($fieldType == Field::TYPE_BLOOD_PRESSURE_TABLE){
                                    $fieldName = $sheetName . ' ' . $fieldName;
                                }

                                $fieldNames[$rowNumber][$n] = $fieldName;
                                $records[$probandId][$fieldName] = $colValue;

                                // Spaltendefinitionen für die Felder einfügen
                                if(!isset($columns[$sheetName][$fieldName])){
                                    $columns[$sheetName][$fieldName] = [
                                        'header' => $fieldName,
                                        'value' => function ($record) use ($fieldName){
                                            return isset($record[$fieldName]) ? $record[$fieldName] : '';
                                        }
                                    ];
                                }
                            }
                        }
                    }
                }else{
                    $fieldName = $value['fieldname'];

                    // Feld-Wert in den entsprechenden Datensatz einfügen
                    $records[$probandId][$fieldName] = $valueContent;

                    // Spaltendefinitionen für die Felder einfügen
                    if(!isset($columns[$sheetName][$fieldName])){
                        $columns[$sheetName][$fieldName] = [
                            'header' => $fieldName,
                            'value' => function ($record) use ($fieldName, $fieldType){
                                $fieldValue = $record[$fieldName];
                                switch($fieldType){
                                    case Field::TYPE_YES_NO:
                                        return $fieldValue == 1 ? 'ja' : 'nein';
                                        break;
                                    case Field::TYPE_DATE:
                                        return date('m/d/Y', intval($fieldValue));
                                        break;
                                    default:
                                        return $fieldValue;
                                }
                            }
                        ];
                    }
                }
            }

            $sheets[$sheetName] = $records;
        }

        return $this->render('export', [
            'sheets' => $sheets,
            'columns' => $columns
        ]);
    }

    /**
     * Finds the Questionnaire model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Questionnaire the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id){
        if(($model = Questionnaire::findOne($id)) !== null){
            return $model;
        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
