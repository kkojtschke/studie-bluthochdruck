<?php

namespace backend\controllers;

use common\models\Proband;
use Yii;
use common\models\User;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller{
    public function behaviors(){
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex(){
        /*
         * Einschränkungen je nach Rolle festlegen.
         * Superadmin: Zugriff auf alle Benutzer
         * Admin: Zugriff auf Biosens und Probanden
         * Biosens: Zugriff auf Probanden, denen er als Biosens zugeordnet ist
         * Proband: kein Zugriff
         */
        $restrictions = [];
        switch(\yii::$app->user->identity->role){
            case User::ROLE_ADMIN:
                $restrictions['role'] = [User::ROLE_BIOSENS];
                break;
        }

        $dataProvider = new ActiveDataProvider([
            'query' => User::find()->where($restrictions),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id){
        return $this->render('view', [
            'user' => $this->findModel($id)
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate(){
        $user = new User();
        $user->generateAuthKey();
        $user->created_at = time();
        $user->updated_at = time();

        if($user->load(Yii::$app->request->post())){
            $password = Yii::$app->request->post()['User']['password'];
            if(!empty($password)){
                $user->setPassword($password);
            }
            $user->save();
            return $this->redirect(['view', 'id' => $user->id]);
        }else{
            return $this->render('create', [
                'user' => $user,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id){
        $user = $this->findModel($id);
        if($user->load(Yii::$app->request->post())){
            $password = Yii::$app->request->post()['User']['password'];
            if(!empty($password)){
                $user->setPassword($password);
            }
            $user->save();
            return $this->redirect(['view', 'id' => $user->id]);
        }else{
            return $this->render('update', [
                'user' => $user,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id){
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id){
        if(($user = User::findOne($id)) !== null){
            return $user;
        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
