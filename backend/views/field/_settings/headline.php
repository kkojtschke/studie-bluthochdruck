<?php

/* @var $this yii\web\View */
/* @var $field common\models\Field */
/* @var $index integer */
/* @var $mode string */
/* @var $settings array */

$level = isset($settings['level']) ? $settings['level'] : 2;
if($mode == 'display'){
    echo '<strong>Ordnung:</strong> ' . $level;
}else{
    $this->registerJs('$(function(){initHeadlineSettings(' . $index . ');});');
    ?>
    <label for="headline-level-<?= $index ?>">Ordnung:</label>
    <select id="headline-level-<?= $index ?>" class="form-control">
        <option value="2">2.</option>
        <option value="3">3.</option>
        <option value="4">4.</option>
    </select>
    <?php
}
