<?php

/* @var $this yii\web\View */
/* @var $field common\models\Field */
/* @var $index integer */
/* @var $mode string */
/* @var $settings array */

$items = isset($settings['items']) ? $settings['items'] : [];
if($mode == 'display'){
    echo '<strong>Listenoptionen:</strong> ' . implode(', ', preg_replace('/([a-z][^,]*)/i', '\'"$1"', $items));
}else{
    $this->registerJs('$(function(){initRadioListSettings(' . $index . ');});');
    ?>
    <label for="csv-list-items-<?= $index ?>">Listenoptionen:</label>
    <p>
        kommasepariert, bspw.: <em>-2, -1, 0, 1, 2</em> oder <em>"Apfel", "Birne", "Banane"</em>
    </p>
    <input type="text" id="csv-list-items-<?= $index ?>" class="form-control"/>
    <?php
}