<?php

/* @var $this yii\web\View */
/* @var $field common\models\Field */
/* @var $index integer */
/* @var $mode string */
/* @var $settings array */

$columns = isset($settings['columns']) ? $settings['columns'] : [];
$addText = isset($settings['addText']) ? $settings['addText'] : 'Neues Medikament';
$maxRows = isset($settings['maxRows']) ? $settings['maxRows'] : 0;

if($mode == 'display'){
    $columnList = '<ul>';
    foreach($columns as $column){
        $columnList .= '<li>' . $column[0] . ' (' . $column[1] . ')';
    }
    $columnList .= '</ul>';

    echo '<strong>Spalten:</strong>' . $columnList;

    echo '<br /><strong>Text für den Hinzufügen-Link:</strong> ' . $addText;

    echo '<br /><br /><strong>Maximale Anzahl Zeilen:</strong> ' . ($maxRows > 0 ? $maxRows : 'unbegrenzt');
}else{
    $this->registerJs('$(function(){initTableSettings(' . $index . ');});');
    ?>
    <table id="column-editor-<?= $index ?>" class="table-editor" data-json-field="helper-input-<?= $index ?>">
        <thead>
        <tr>
            <th>Spaltenüberschrift</th>
            <th>Spaltentitel in der Exceldatei<br />(<strong>Platzhalter:</strong> %N: Zeilennummer)</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><input type="text" class="form-control"/></td>
            <td><input type="text" class="form-control"/></td>
            <td><a href="#" class="remove">Entfernen</a></td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="4"><a href="#" class="add"> Neue Spalte</a></td>
        </tr>
        </tfoot>
    </table>

    <input type="hidden" name="helper-input-<?= $index ?>"/>

    <label for="add-text">Text für den Hinzufügen-Link</label> <input type="text" id="add-text" value="<?= $addText ?>"/>

    <label for="max-rows">Maximale Anzahl Zeilen (0 = unbegrenzt)</label>
    <input type="text" id="max-rows" value="<?= $maxRows ?>"/>

    <?php
}