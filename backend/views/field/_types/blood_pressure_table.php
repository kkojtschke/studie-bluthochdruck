<?php

/* @var $value common\models\Value */
/* @var $field common\models\Field */
/* @var $activeField yii\widgets\ActiveField */
/* @var $htmlAttributes array */
/* @var $settings array */

$attr = $htmlAttributes + [
        'class' => 'table blood-pressure-table-editor',
        'data-json-field' => 'Value[' . $field->sorting . '][content]',
    ];
$attrString = '';
foreach($attr as $attrName => $attrValue){
    $attrString .= ' ' . $attrName . '="' . $attrValue . '"';
}

$tableHtml = '<table' . $attrString . '>';
$tableHtml .= '<thead>';

$tableHtml .= '<tr>';
$tableHtml .= '<th>Tag</th>';
$tableHtml .= '<th>RR morgens</th>';
$tableHtml .= '<th>RR mittags</th>';
$tableHtml .= '<th>RR abends</th>';
$tableHtml .= '</tr>';

$tableHtml .= '</thead>';
$tableHtml .= '<tbody>';

for($i = 1; $i <= 3; ++$i){
    $tableHtml .= '<tr>';
    $tableHtml .= '<td>' . $i . '.</td>';
    $tableHtml .= '<td><input type="text" /></td>';
    $tableHtml .= '<td><input type="text" /></td>';
    $tableHtml .= '<td><input type="text" /></td>';
    $tableHtml .= '</tr>';
}

$tableHtml .= '</tbody>';
$tableHtml .= '</table>';

$activeField->hiddenInput()->template = "{label}\n{table}\n{input}\n{hint}\n{error}";
$activeField->parts['{table}'] = $tableHtml;