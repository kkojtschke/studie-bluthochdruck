<?php
/* @var $value common\models\Value */
/* @var $field common\models\Field */
/* @var $activeField yii\widgets\ActiveField */
/* @var $htmlAttributes array */
/* @var $settings array */

$activeField = $activeField->widget(\yii\jui\DatePicker::className(), [
    'language' => 'de',
    'dateFormat' => 'dd.MM.yyyy',
    'options' => $htmlAttributes
]);