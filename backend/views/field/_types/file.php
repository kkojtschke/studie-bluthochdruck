<?php

use yii\helpers\Html;

/* @var $value common\models\Value */
/* @var $field common\models\Field */
/* @var $activeField yii\widgets\ActiveField */
/* @var $htmlAttributes array */
/* @var $settings array */

$activeField->fileInput($htmlAttributes + ['value' => false])
    ->template = "{label}\n{info}\n{input}\n{hint}\n{error}";
$additionalData = $value->getAdditonalDataAsArray();
$fileName = isset($additionalData['fileName'])
    ? $additionalData['fileName'] . Html::a(' (ansehen)',
        ['/proband/view-file', 'proband_id' => $value->proband_id, 'field_id' => $value->field_id],
        ['target' => '_blank'])
    : null;
$info = 'Nur PDF oder JPEG<br />' . $fileName;
$activeField->parts['{info}'] = '<div>' . $info . '</div>';