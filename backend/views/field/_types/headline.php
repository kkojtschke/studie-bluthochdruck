<?php

use \yii\helpers\Html;

/* @var $value common\models\Value */
/* @var $field common\models\Field */
/* @var $activeField yii\widgets\ActiveField */
/* @var $htmlAttributes array */
/* @var $settings array */

$activeField->input([])->template = Html::tag(
    'h' . ($settings['level'] ? $settings['level'] : 2),
    $field->label,
    $htmlAttributes + ['name' => 'Value[' . $field->sorting . '][content]']
);