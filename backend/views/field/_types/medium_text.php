<?php
/* @var $value common\models\Value */
/* @var $field common\models\Field */
/* @var $activeField yii\widgets\ActiveField */
/* @var $htmlAttributes array */
/* @var $settings array */

$activeField->textarea($htmlAttributes + ['maxlength' => true,
        'value' => $value->content ? $value->content : $field->default_value]);