<?php
/* @var $value common\models\Value */
/* @var $field common\models\Field */
/* @var $activeField yii\widgets\ActiveField */
/* @var $htmlAttributes array */
/* @var $settings array */

$columns = [];
if(isset($settings['items'])){
    // die Array-Keys sollen genauso lauten wie die Values
    foreach($settings['items'] as $column){
        $columns[$column] = $column;
    }
}
$activeField->radioList($columns, $htmlAttributes);