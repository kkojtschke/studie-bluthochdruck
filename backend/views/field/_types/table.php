<?php
/* @var $value common\models\Value */
/* @var $field common\models\Field */
/* @var $activeField yii\widgets\ActiveField */
/* @var $htmlAttributes array */
/* @var $settings array */

$columns = [];
if(isset($settings['columns'])){
    // die Array-Keys sollen genauso lauten wie die Values
    foreach($settings['columns'] as $column){
        $columns[$column[0]] = $column[0];
    }
}

$addText = isset($settings['addText']) ? $settings['addText'] : 'Neues Medikament';

$attr = $htmlAttributes + [
        'class' => 'table table-editor',
        'data-json-field' => 'Value[' . $field->sorting . '][content]',
    ];
if(isset($settings['maxRows'])){
    $attr['data-max-rows'] = $settings['maxRows'];
}

$attrString = '';
foreach($attr as $attrName => $attrValue){
    $attrString .= ' ' . $attrName . '="' . $attrValue . '"';
}

$tableHtml = '<table' . $attrString . '>';
$tableHtml .= '<thead>';

$tableHtml .= '<tr>';
foreach($columns as $column){
    $tableHtml .= '<th>' . $column . '</th>';
}
$tableHtml .= '</tr>';

$tableHtml .= '</thead>';
$tableHtml .= '<tbody>';

$tableHtml .= '<tr>';
foreach($columns as $column){
    $tableHtml .= '<td><input type = "text" /></td>';
}
$tableHtml .= '<td><a href = "#" class="remove">Entfernen</a></td>';
$tableHtml .= '</tr>';

$tableHtml .= '</tbody>';
$tableHtml .= '<tfoot>';

$tableHtml .= '<tr>';
$tableHtml .= '<td colspan = "4"><a href = "#" class="add" > ' . $addText . ' </a ></td>';
$tableHtml .= '</tr>';

$tableHtml .= '</tfoot>';
$tableHtml .= '</table>';

$activeField->hiddenInput()->template = "{label}\n{table}\n{input}\n{hint}\n{error}";
$activeField->parts['{table}'] = $tableHtml;