<?php
/* @var $value common\models\Value */
/* @var $field common\models\Field */
/* @var $activeField yii\widgets\ActiveField */
/* @var $htmlAttributes array */
/* @var $settings array */

$activeField->radioList([0 => 'nein', 1 => 'ja'], $htmlAttributes);