<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Field */

$questionnaire = common\models\Questionnaire::findOne(['id' => $model->questionnaire_id]);
$survey = common\models\Survey::findOne(['id' => $questionnaire->survey_id]);

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Studie: ' . $survey->title, 'url' => ['survey/view',
    'id' => $questionnaire->survey_id]];
$this->params['breadcrumbs'][] = ['label' => 'Fragebogen: ' . $questionnaire->title, 'url' => ['questionnaire/update',
    'id' => $model->questionnaire_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="field-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
