<?php
use yii\helpers\Html;
use yii\helpers\Json;

/* @var $proband common\models\Proband */
/* @var $questionnaires array<common\models\Questionnaire> */

$filled = [];
$notFilled = [];

foreach($questionnaires as $questionnaire){
    $button = Html::a($questionnaire->title, ['questionnaire/fill', 'proband_id' => $proband->id,
        'id' => $questionnaire->id], ['class' => 'btn btn-primary']);

    $filled_questionnaires = Json::decode($proband->filled_questionnaires);
    if($filled_questionnaires == null){
        $filled_questionnaires = [];
    }
    if(in_array($questionnaire->id, $filled_questionnaires)){
        $filled[] = $button;
    }else{
        $notFilled[] = $button;
    }
}

?>
<div class=" row">
    <div class="col-md-6">
        <?php
        if(!empty($notFilled)){
            echo '<h3>Noch nicht eingegeben:</h3>';
            foreach($notFilled as $button){
                echo $button;
            }
        }
        ?>
    </div>
    <div class="col-md-6">
        <?php
        if(!empty($filled)){
            echo '<h3>Bereits eingegeben:</h3>';
            foreach($filled as $button){
                echo $button;
            }
        }
        ?>
    </div>
</div>
