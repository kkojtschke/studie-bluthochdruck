<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $proband common\models\Proband */
/* @var $form yii\widgets\ActiveForm */

if($proband->isNewRecord){
    $this->title = 'Registrierung eines Probanden in der BioMeZ-Studie "Biomeditation bei Bluthochdruck" 2015/2016';
    $this->params['breadcrumbs'][] = 'Proband registieren';
}else{
    $this->title = 'Probandendaten bearbeiten';
    $this->params['breadcrumbs'][] = $this->title;
}
?>

<div class="proband-form">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    if($proband->isNewRecord){
        ?>
        <script type="text/javascript">
            $(function(){
                var $mandatoryQuestions = $('[name^=mandatory-question-]');
                $mandatoryQuestions.change(function(){
                    if($('[name^=mandatory-question-1]:checked').prop('value') == 1 &&
                        $('[name^=mandatory-question-2]:checked').prop('value') == 1
                    ){
                        $('form').removeClass('hidden');
                        $('#mandatory-message').hide();
                    }else{
                        $('form').addClass('hidden');
                        $('#mandatory-message').show();
                    }
                });
            });
        </script>

        <div class="form-group">
            <label>
                Hat ein Arzt bei Deinem Probanden einen <strong>Bluthochdruck</strong> diagnostiziert?
            </label>
            <input type="radio" name="mandatory-question-1" value="0" checked="checked"/> nein
            <input type="radio" name="mandatory-question-1" value="1"/> ja
        </div>

        <div class="form-group">
            <label>
                Hat oder hatte Dein Proband (vor Einnahme blutdrucksenkender Medikamente) durchschnittlich systolische
                Blutdruckwerte von über 160 mm Hg?
            </label>
            <input type="radio" name="mandatory-question-2" value="0" checked="checked"/> nein
            <input type="radio" name="mandatory-question-2" value="1"/> ja
        </div>

        <div id="mandatory-message" class="alert alert-danger">
            Nur wenn beide Fragen mit "ja" beantwortet werden, darf die Registrierung genehmigt werden.<br/>
            Voraussetzung für die Teilnahme an der Studie ist, dass bei Deinem Probanden ein sogenannter mittlerer oder
            schwerer Blutdruck vorliegt, d. h. mit Blutdruckwerten von systolisch > 160 oder > 180 mm Hg.
        </div>
    <?php } ?>

    <?php $form = ActiveForm::begin([
        'options' => [
            'class' => $proband->isNewRecord ? 'hidden' : '',
            'enctype' => 'multipart/form-data'
        ]
    ]); ?>

    <?= $form->field($proband, 'title')->textInput() ?>
    <?= $form->field($proband, 'last_name')->textInput() ?>
    <?= $form->field($proband, 'first_name')->textInput() ?>
    <?= $form->field($proband, 'date_of_birth')->widget(\yii\jui\DatePicker::className(), [
        'options' => ['class' => 'form-control'],
        'model' => $proband,
        'attribute' => 'date_of_birth',
        'language' => 'de',
        'dateFormat' => 'dd.MM.yyyy',
    ]) ?>
    <?= $form->field($proband, 'sex')->radioList([\common\models\User::SEX_MALE => 'männlich',
        \common\models\User::SEX_FEMALE => 'weiblich']) ?>
    <?= $form->field($proband, 'street')->textInput() ?>
    <?= $form->field($proband, 'street_number')->textInput() ?>
    <?= $form->field($proband, 'zip_code')->textInput() ?>
    <?= $form->field($proband, 'city')->textInput() ?>
    <?= $form->field($proband, 'country')->widget(\yii\jui\AutoComplete::classname(), [
        'options' => ['class' => 'form-control'],
        'clientOptions' => [
            'source' => ['Deutschland', 'Österreich']
        ],
    ]) ?>
    <?= $form->field($proband, 'phone_prefix') ?>
    <?= $form->field($proband, 'phone_number') ?>
    <?= $form->field($proband, 'email')->textInput() ?>
    <?= $form->field($proband, 'month_and_year_of_diagnosis')->textInput() ?>

    <h2>Angaben zu etwaigen Biomeditationssitzungen des Probanden</h2>

    <?= $form->field($proband, 'has_yet_attended_a_treatment')->radioList([0 => 'nein', 1 => 'ja']) ?>
    <?= $form->field($proband, 'time_since_last_treatment')->textInput([
        'data-dependent-on' => 'Proband[has_yet_attended_a_treatment]'
    ])->label('Wann hatte Dein Proband die letzte Sitzung?<br />' .
        'Die letzte Sitzung hatte mein Proband vor ...<br />' .
        'Bsp.: 14 Tagen oder 2 Monaten') ?>
    <?= $form->field($proband, 'count_of_treatments')->textInput([
        'data-dependent-on' => 'Proband[has_yet_attended_a_treatment]'
    ])->label('Wieviele Sitzungen hatte Dein Proband in etwa <em>insgesamt</em>?') ?>

    <h2>Anmeldebögen hochladen</h2>
    <?= $form->field($proband, 'upload_einverstaendniserklaerung')->fileInput() ?>
    <?= $form->field($proband, 'upload_eignungsbogen')->fileInput() ?>
    <?= $form->field($proband, 'how_took_notice') ?>

    <div class="form-group">
        <?= Html::submitButton('Speichern',
            ['class' => $proband->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>