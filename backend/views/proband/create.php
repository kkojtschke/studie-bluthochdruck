<?php

use yii\helpers\Html;
use common\models\User;


/* @var $this yii\web\View */
/* @var $proband common\models\Proband */

$this->title = 'Benutzer anlegen';
$this->params['breadcrumbs'][] = ['label' => 'Probanden', 'url' => ['index']];

?>
<div class="user-create">
    <?php
        echo $this->render('_form', [
            'proband' => $proband
        ]);
    ?>
</div>
