<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $proband common\models\Proband */
/* @var $questionnaires array<common\models\Questionnaire> */

$this->title = $proband->first_name . ' ' . $proband->last_name . ': Ersterhebung';
$this->params['breadcrumbs'][] = ['label' => 'Probanden verwalten', 'url' => ['proband/index']];
$this->params['breadcrumbs'][] = ['label' => $proband->first_name . ' ' . $proband->last_name, 'url' =>
    ['proband/view', 'id' => $proband->id]];
$this->params['breadcrumbs'][] = 'Ersterhebung';
?>

<div class="proband-ersterhebung">
    <div class="panel panel-default">
        <div class="panel-heading"><h1><?= Html::encode($this->title) ?></h1></div>
        <div class="panel-body">
            <p>
                <?= $this->render('_buttons_boegen', [
                    'proband' => $proband,
                    'questionnaires' => $questionnaires
                ]) ?>
            </p>
        </div>
    </div>
</div>