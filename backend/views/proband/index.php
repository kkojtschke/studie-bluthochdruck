<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \common\models\User;
use \common\models\Questionnaire;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Probanden verwalten';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proband-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Proband registrieren', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Registrierungsdaten exportieren', ['/questionnaire/export',
            'group' => Questionnaire::GROUP_REGISTRIERUNG], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Ersterhebung exportieren', ['/questionnaire/export', 'group' => Questionnaire::GROUP_ERSTERHEBUNG],
            ['class' => 'btn btn-success']) ?>
        <?= Html::a('Nacherhebung exportieren', ['/questionnaire/export', 'group' => Questionnaire::GROUP_NACHERHEBUNG],
            ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'title',
            'first_name',
            'last_name',
            'email:email',
            [
                'attribute' => 'sex',
                'content' => function ($model){
                    return $model->sex == User::SEX_MALE ? 'männlich' : 'weiblich';
                }
            ],
            [
                'attribute' => 'created_at',
                'content' => function ($model){
                    return date('d.m.y', $model->created_at);
                }
            ],
            'street',
            'street_number',
            'zip_code',
            'city',
            'country',
            [
                'attribute' => 'date_of_birth',
                'content' => function ($model){
                    return date('d.m.y', $model->date_of_birth);
                }
            ],

            [
                'content' => function ($model) {
                    if($model->admitted === NULL) {
                        return Html::a('Zulassen', ['admit', 'id' => $model->id], ['class' => 'btn btn-success']) .
                        Html::a('Ablehnen', ['deny', 'id' => $model->id], ['class' => 'btn btn-danger']);
                    } else {
                        if($model->admitted == 1) {
                            return 'zugelassen ' . Html::a('Ablehnen', ['deny', 'id' => $model->id],
                                ['class' => 'btn btn-danger']);
                        } else {
                            return 'abgelehnt ' . Html::a('Zulassen', ['admit', 'id' => $model->id],
                                ['class' => 'btn btn-success']);
                        }
                    }
                }
            ],

            [
                'content' => function ($model){
                    return Html::a('Ersterhebung', ['ersterhebung', 'id' => $model->id],
                        ['class' => 'btn btn-primary']);
                }
            ],
            [
                'content' => function ($model){
                    return Html::a('Nacherhebung', ['nacherhebung', 'id' => $model->id],
                        ['class' => 'btn btn-primary']);
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
