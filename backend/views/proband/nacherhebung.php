<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $proband common\models\Proband */
/* @var $studie_abgebrochen boolean */
/* @var $questionnaires array<common\models\Questionnaire> */

$this->title = $proband->first_name . ' ' . $proband->last_name . ': Nacherhebung';
$this->params['breadcrumbs'][] = ['label' => 'Probanden verwalten', 'url' => ['proband/index']];
$this->params['breadcrumbs'][] = ['label' => $proband->first_name . ' ' . $proband->last_name, 'url' =>
    ['proband/view', 'id' => $proband->id]];
$this->params['breadcrumbs'][] = 'Nacherhebung';
?>

<div class="proband-nacherhebung">
    <div class="panel panel-default">
        <div class="panel-heading"><h1><?= Html::encode($this->title) ?></h1></div>
        <div class="panel-body">
            <?php
            if($studie_abgebrochen === null){
                ?>
                <h2>Hat der Proband die Studie abgebrochen?
                    <?= Html::a('Nein', ['nacherhebung', 'id' => $proband->id, 'studie_abgebrochen' => false],
                        ['class' => 'btn btn-success']) ?>
                    <?= Html::a('Ja', ['nacherhebung', 'id' => $proband->id, 'studie_abgebrochen' => true],
                        ['class' => 'btn btn-danger']) ?>
                </h2>
                <?php
            }else{
                echo $this->render('_buttons_boegen', [
                    'proband' => $proband,
                    'questionnaires' => $questionnaires
                ]);
            }
            ?>
        </div>
    </div>
</div>