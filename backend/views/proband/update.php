<?php

/* @var $this yii\web\View */
/* @var $proband common\models\Proband */

$this->title = $proband->first_name . ' ' . $proband->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Probanden verwalten', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $proband->first_name . ' ' . $proband->last_name, 'url' => ['view', 'id' => $proband->id]];
?>
<div class="proband-update">
    <?php
        echo $this->render('_form', [
            'proband' => $proband,
        ]);
    ?>

</div>
