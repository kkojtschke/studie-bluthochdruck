<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\User;

/* @var $this yii\web\View */
/* @var $proband common\models\Proband */
/* @var $questionnaires array<common\models\Questionnaire> */

$this->title = $proband->first_name . ' ' . $proband->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Probanden verwalten', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proband-view">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1>
                <?= Html::encode($this->title) ?>
                <?= Html::a('Bearbeiten', ['update', 'id' => $proband->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Ersterhebung', ['ersterhebung', 'id' => $proband->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Nacherhebung', ['nacherhebung', 'id' => $proband->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Löschen', ['delete', 'id' => $proband->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Wollen Sie den Probanden wirklich löschen?',
                        'method' => 'post',
                    ],
                ]) ?>
            </h1>
        </div>
        <div class="panel-body">
            <p>
                <?php
                if($proband->admitted === NULL) {
                    echo Html::a('Zulassen', ['admit', 'id' => $proband->id], ['class' => 'btn btn-success']) .
                        Html::a('Ablehnen', ['deny', 'id' => $proband->id], ['class' => 'btn btn-danger']);
                } else {
                    if($proband->admitted == 1) {
                        echo 'zugelassen ' . Html::a('Ablehnen', ['deny', 'id' => $proband->id],
                                ['class' => 'btn btn-danger']);
                    } else {
                        echo 'abgelehnt ' . Html::a('Zulassen', ['admit', 'id' => $proband->id],
                                ['class' => 'btn btn-success']);
                    }
                }
                ?>
            </p>
            <p>
                <?= $this->render('_buttons_boegen', [
                    'proband' => $proband,
                    'questionnaires' => $questionnaires
                ]) ?>
            </p>
        </div>
    </div>

    <?= DetailView::widget([
        'model' => $proband,
        'attributes' => [
            'id',
            [
                'label' => 'Voller Name',
                'value' => trim($proband->title . ' ' . $proband->first_name . ' ' . $proband->last_name)
            ],
            [
                'attribute' => 'sex',
                'value' => $proband->sex == \common\models\User::SEX_MALE ? 'männlich' : 'weiblich'
            ],
            [
                'label' => 'Adresse',
                'value' => $proband->street . ' ' . $proband->street_number . ', ' . $proband->zip_code . ' '
                    . $proband->city . ', ' . $proband->country
            ],
            [
                'attribute' => 'date_of_birth',
                'value' => date('d.m.y', $proband->date_of_birth)
            ],
            'email:email',
            [
                'attribute' => 'created_at',
                'value' => date('d.m.y', $proband->created_at)
            ],
            [
                'label' => 'Telefonnummer',
                'value' => $proband->phone_prefix . ' ' . $proband->phone_number
            ],
            'month_and_year_of_diagnosis',
            'has_yet_attended_a_treatment:boolean',
            'time_since_last_treatment',
            'count_of_treatments',
            'how_took_notice',
        ],
    ]) ?>

    <?= DetailView::widget([
        'model' => $proband,
        'attributes' => [
            [
                'attribute' => 'upload_einverstaendniserklaerung',
                'format' => 'raw',
                'value' => $proband->upload_einverstaendniserklaerung ? Html::a('Ansehen',
                    ['einverstaendniserklaerung', 'id' => $proband->id], ['target' => '_blank']) : ''
            ],
            [
                'attribute' => 'upload_eignungsbogen',
                'format' => 'raw',
                'value' => $proband->upload_eignungsbogen ? Html::a('Ansehen',
                    ['eignungsbogen', 'id' => $proband->id], ['target' => '_blank']) : ''
            ],
            [
                'label' => 'Biosens',
                'value' => User::findIdentity($proband->biosens_id)->first_name . ' '
                    . User::findIdentity($proband->biosens_id)->last_name
            ],
            'email:email',
            [
                'attribute' => 'created_at',
                'value' => date('d.m.y', $proband->created_at)
            ],
            [
                'attribute' => 'updated_at',
                'value' => date('d.m.y', $proband->updated_at)
            ],
        ],
    ]) ?>

</div>
