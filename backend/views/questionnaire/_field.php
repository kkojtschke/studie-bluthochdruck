<?php

use common\models\Field;
use \yii\helpers\Json;

/* @var $value common\models\Value */
/* @var $form yii\widgets\ActiveForm */

$field = $value->field;
$activeField = $form->field($value, "[$field->sorting]content");
$htmlAttributes = [];
// anhängige Felder erstellen
if($field->condition){
    $dependingField = Field::findOne($field->condition);
    $htmlAttributes += [
        'data-dependent-on' => 'Value[' . $dependingField->sorting . '][content]'
    ];
}

$settings = Json::decode($field->settings);

// den Feld-Typ in einen String umwandeln
$fieldClass = new ReflectionClass('common\models\Field');
$constants = $fieldClass->getConstants();

$typeName = null;
foreach($constants as $name => $v){
    if($v == $field->type){
        $typeName = strtolower(str_replace('TYPE_', '', $name));
        break;
    }
}

$this->render('/field/_types/' . $typeName, [
    'value' => $value,
    'field' => $field,
    'activeField' => $activeField,
    'htmlAttributes' => $htmlAttributes,
    'settings' => $settings
]);

$activeField->label($field->label ? $field->label : false);

echo $activeField;

