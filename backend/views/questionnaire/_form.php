<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use kartik\editable\Editable;
use common\models\Field;
use common\models\Questionnaire;
use yii\helpers\Json;

/* @var $this yii\web\View */
/* @var $questionnaire common\models\Questionnaire */
/* @var $fields yii\data\ActiveDataProvider */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="questionnaire-form">

    <?php $form = ActiveForm::begin(); ?>

    <h1><?= Html::encode($this->title) . ' ' . Html::submitButton($questionnaire->isNewRecord ? 'Anlegen' : 'Speichern',
            ['class' => $questionnaire->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?></h1>

    <?= $form->field($questionnaire, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($questionnaire, 'group')->dropDownList(Questionnaire::$groupLabels) ?>

    <?= $form->field($questionnaire, 'export_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($questionnaire, 'hide_when_cancelled')->radioList([0 => 'Nein', 1 => 'Ja']) ?>

    <?php ActiveForm::end(); ?>

    <?php
    $defaultEditableOptions = [
        'preHeader' => '<i class="glyphicon glyphicon-edit"></i> ',
        'header' => 'Bearbeiten',
        'formOptions' => [
            'action' => ['field/update']
        ],
        'pluginEvents' => [
            "editableSuccess" => "function(event, val, form, data){if(data.reload == true) window.location.reload();}",
        ]
    ];

    if(!$questionnaire->isNewRecord){
        echo GridView::widget([
            'dataProvider' => $fields,
            'dataColumnClass' => '\kartik\grid\EditableColumn',
            'layout' => '{toolbar}{items}{toolbar}{pager}',
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i> Neues Feld anlegen',
                            ['field/create', 'questionnaire_id' => $questionnaire->id],
                            [
                                'title' => 'Neues Feld anlegen',
                                'class' => 'btn btn-success'
                            ])
                ]
            ],
            'columns' => [
                [
                    'attribute' => 'id',
                    'readonly' => true
                ],
                [
                    'attribute' => 'name',
                    'editableOptions' => array_merge_recursive(
                        $defaultEditableOptions,
                        []
                    )
                ],
                [
                    'attribute' => 'label',
                    'editableOptions' => array_merge_recursive(
                        $defaultEditableOptions,
                        [
                            'valueIfNull' => '<em>keine</em>'
                        ]
                    )
                ],
                [
                    'attribute' => 'type',
                    'editableOptions' => array_merge_recursive(
                        $defaultEditableOptions,
                        [
                            'inputType' => Editable::INPUT_DROPDOWN_LIST,
                            'data' => Field::$typeLabels,
                            'displayValueConfig' => Field::$typeLabels
                        ]
                    )
                ],
                [
                    'attribute' => 'default_value',
                    'editableOptions' => array_merge_recursive(
                        $defaultEditableOptions,
                        [
                            'valueIfNull' => '<em>keiner</em>'
                        ]
                    ),
                    // diese Spalte für Überschriften deaktivieren
                    'readonly' => function ($field){
                        switch($field->type){
                            case Field::TYPE_HEADLINE:
                                return true;
                                break;
                            default:
                                return false;
                        }
                    }
                ],
                [
                    'attribute' => 'condition',
                    'editableOptions' => function ($field, $key, $index) use ($questionnaire, $defaultEditableOptions){
                        $fieldsForDisplay =
                            ArrayHelper::map(
                                Field::find()
                                    ->where(['in', 'id', array_keys($questionnaire->fields)])
                                    ->andWhere(['!=', 'id', $field->id])
                                    ->andWhere(['type' => Field::TYPE_YES_NO])
                                    ->orderBy('id')->select('id, name')
                                    ->asArray(true)->all(),
                                'id',
                                function ($field){
                                    return $field['name'];
                                }
                            );

                        return array_merge_recursive(
                            $defaultEditableOptions,
                            [
                                'valueIfNull' => '<em>keinem Feld</em>',
                                'inputType' => Editable::INPUT_DROPDOWN_LIST,
                                'data' => [null => '-'] + $fieldsForDisplay,
                                'displayValueConfig' => [null => '<em>keinem Feld</em>'] + $fieldsForDisplay
                            ]
                        );
                    }
                ],
                [
                    'attribute' => 'settings',
                    'editableOptions' => function ($field, $key, $index) use ($questionnaire, $defaultEditableOptions){
                        // den Feld-Typ in einen String umwandeln
                        $fieldClass = new ReflectionClass('common\models\Field');
                        $constants = $fieldClass->getConstants();

                        $typeName = null;
                        foreach($constants as $name => $v){
                            if($v == $field->type){
                                $typeName = strtolower(str_replace('TYPE_', '', $name));
                                break;
                            }
                        }

                        $settingsPath = '/field/_settings/' . $typeName;

                        $settings = Json::decode($field->settings);

                        return
                            array_merge_recursive(
                                $defaultEditableOptions,
                                [
                                    'valueIfNull' => false,
                                    'inputType' => Editable::INPUT_HIDDEN,
                                    'size' => 'lg',
                                    'displayValue' => $this->render($settingsPath, [
                                        'index' => $index,
                                        'mode' => 'display',
                                        'field' => $field,
                                        'settings' => $settings
                                    ]),
                                    'beforeInput' => function () use ($field, $index, $settingsPath, $settings){
                                        return $this->render($settingsPath, [
                                            'index' => $index,
                                            'mode' => 'edit',
                                            'field' => $field,
                                            'settings' => $settings
                                        ]);
                                    }
                                ]
                            );
                    },
                    // diese Spalte nur für bestimmte Feldtypen aktivieren
                    'readonly' => function ($field){
                        switch($field->type){
                            case Field::TYPE_RADIO_LIST:
                            case Field::TYPE_TABLE:
                            case Field::TYPE_HEADLINE:
                                return false;
                                break;
                            default:
                                return true;
                        }
                    }
                ],
                [
                    'attribute' => 'required',
                    'editableOptions' => array_merge_recursive(
                        $defaultEditableOptions,
                        [
                            'inputType' => Editable::INPUT_RADIO_LIST,
                            'data' => Field::$requiredLabels,
                            'displayValueConfig' => Field::$requiredLabels
                        ]
                    ),
                    // diese Spalte für Überschriften deaktivieren
                    'readonly' => function ($field){
                        switch($field->type){
                            case Field::TYPE_HEADLINE:
                                return true;
                                break;
                            default:
                                return false;
                        }
                    }
                ],
                [
                    'attribute' => 'exportable',
                    'editableOptions' => array_merge_recursive(
                        $defaultEditableOptions,
                        [
                            'inputType' => Editable::INPUT_RADIO_LIST,
                            'data' => Field::$exportableLabels,
                            'displayValueConfig' => Field::$exportableLabels
                        ]
                    )
                ],
                [
                    'class' => '\kartik\grid\ActionColumn',
                    'header' => 'Sortieren',
                    'template' => '{moveTop}{moveUp}{moveDown}{moveBottom} {clone} {delete}',
                    'buttons' => [
                        'moveTop' => function ($url, $field){
                            return Html::a('<i class="glyphicon glyphicon-backward" style="transform: rotate(90deg);"></i>',
                                ['field/move-top', 'id' => $field->id]);
                        },
                        'moveUp' => function ($url, $field){
                            return Html::a('<i class="glyphicon glyphicon-chevron-up"></i>',
                                ['field/move-up', 'id' => $field->id]);
                        },
                        'moveDown' => function ($url, $field){
                            return Html::a('<i class="glyphicon glyphicon-chevron-down"></i>',
                                ['field/move-down', 'id' => $field->id]);
                        },
                        'moveBottom' => function ($url, $field){
                            return Html::a('<i class="glyphicon glyphicon-forward" style="transform: rotate(90deg);"></i>',
                                ['field/move-bottom', 'id' => $field->id]);
                        },
                        'delete' => function ($url, $field){
                            return Html::a('<i class="glyphicon glyphicon-remove text-danger"></i>',
                                ['field/delete', 'id' => $field->id]);
                        },
                        'clone' => function ($url, $field){
                            return Html::a('<i class="glyphicon glyphicon-duplicate"></i>',
                                ['field/create', 'questionnaire_id' => $field->questionnaire_id,
                                    'copy_from_field_id' => $field->id]);
                        }
                    ]
                ]
            ],
            'export' => false,
            'responsive' => true,
            'hover' => true
        ]);
    }
    ?>
</div>
