<?php

/* @var $this yii\web\View */
/* @var $questionnaire common\models\Questionnaire */

$survey = common\models\Survey::findOne(['id' => $questionnaire->survey_id]);

$this->title = 'Neuen Fragebogen anlegen';
$this->params['breadcrumbs'][] = ['label' => 'Studie: ' . $survey->title, 'url' => ['survey/update',
    'id' => $questionnaire->survey_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questionnaire-create">
    <?= $this->render('_form', [
        'questionnaire' => $questionnaire,
    ]) ?>
</div>
