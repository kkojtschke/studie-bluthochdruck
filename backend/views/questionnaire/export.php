<?php

use \moonland\phpexcel\Excel;

/* @var $this yii\web\View */
/* @var $sheets array */
/* @var $columns array */

Excel::widget([
    'models' => $sheets,
    'isMultipleSheet' => true,
    'mode' => 'export',
    'columns' => $columns
]);