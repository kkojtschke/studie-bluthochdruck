<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \common\models\Questionnaire;

/* @var $this yii\web\View */
/* @var $questionnaire common\models\Questionnaire */
/* @var $proband common\models\Proband */
/* @var $values array<common\models\Value> */

$this->title = $proband->first_name . ' ' . $proband->last_name . ': ' . $questionnaire->title;
$this->params['breadcrumbs'][] = ['label' => 'Probanden verwalten', 'url' => ['proband/index']];
$this->params['breadcrumbs'][] = ['label' => $proband->first_name . ' ' . $proband->last_name, 'url' =>
    ['proband/view', 'id' => $proband->id]];

if($questionnaire->group != Questionnaire::GROUP_REGISTRIERUNG){
    $groupLabel = Questionnaire::$groupLabels[$questionnaire->group];
    $this->params['breadcrumbs'][] = ['label' => $groupLabel,
        'url' => ['proband/' . strtolower($groupLabel), 'id' => $proband->id]];
}

$this->params['breadcrumbs'][] = $questionnaire->title;
?>
<div class="questionnaire-fill">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data'
        ]
    ]); ?>

    <?php
    $saveButton = Html::submitButton('Speichern',
        ['class' => 'btn btn-primary']);
    ?>

    <h1>
        <?= Html::encode($this->title) . ' ' . $saveButton ?>
    </h1>

    <?php
    foreach($values as $value){
        echo $this->render('_field', [
            'value' => $value,
            'form' => $form
        ]);
    }
    ?>

    <?= $saveButton ?>

    <?php ActiveForm::end(); ?>

</div>
