<?php

/* @var $this yii\web\View */
/* @var $questionnaire common\models\Questionnaire */
/* @var $fields yii\data\ActiveDataProvider */

$survey = common\models\Survey::findOne(['id' => $questionnaire->survey_id]);

$this->title = $questionnaire->title;
$this->params['breadcrumbs'][] = ['label' => 'Studie: ' . $survey->title, 'url' => ['survey/update',
    'id' => $questionnaire->survey_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questionnaire-update">
    <?= $this->render('_form', [
        'questionnaire' => $questionnaire,
        'fields' => $fields
    ]) ?>
</div>
