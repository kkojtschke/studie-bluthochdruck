<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Survey */
/* @var $form yii\widgets\ActiveForm */
/* @var $questionaires yii\data\ActiveDataProvider */
?>

<div class="survey-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'start_time')->textInput() ?>

    <?= $form->field($model, 'end_time')->textInput() ?>

    <div>
        <h2>Fragebögen:</h2>
        <?= Html::a('Neuen Fragebogen anlegen', ['questionnaire/create', 'survey_id' => $model->id],
            ['class' => 'btn btn-primary']) ?>
        <?= GridView::widget([
            'dataProvider' => $questionnaires,
            'columns' => [
                'title',
                [
                    'attribute' => 'group',
                    'content' => function($questionnaire){
                        return \common\models\Questionnaire::$groupLabels[$questionnaire->group];
                    }
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'controller' => 'questionnaire',
                    'template' => '{update} {clone} {delete}',
                    'buttons' => [
                        'clone' => function ($url, $questionnaire) use ($model){
                            return Html::a('<i class="glyphicon glyphicon-duplicate"></i>',
                                ['questionnaire/create', 'survey_id' => $model->id,
                                    'copy_from_questionnaire_id' => $questionnaire->id]);
                        }
                    ]
                ],
            ],
        ]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
