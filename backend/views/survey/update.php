<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Survey */
/* @var $questionaires yii\data\ActiveDataProvider */

$this->title = 'Studie: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Studien', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;
?>
<div class="survey-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'questionnaires' => $questionnaires
    ]) ?>

</div>
