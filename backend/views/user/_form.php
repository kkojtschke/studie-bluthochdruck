<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $user common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($user, 'first_name')->textInput() ?>
    <?= $form->field($user, 'last_name')->textInput() ?>
    <?= $form->field($user, 'email')->textInput() ?>
    <?= $form->field($user, 'password')->passwordInput()
        ->label('Passwort (leer lassen, wenn es nicht geändert werden soll)') ?>

    <?php
    if(!$user->role){
        echo $form->field($user, 'role')->dropDownList(\common\models\User::getAvailableRoles());
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton('Speichern', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>