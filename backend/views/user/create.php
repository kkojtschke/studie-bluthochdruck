<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$this->title = 'Benutzer anlegen';
$this->params['breadcrumbs'][] = ['label' => 'Benutzer', 'url' => ['index']];
?>
<div class="user-create">
    <?php
    echo $this->render('_form', [
        'user' => $user
    ]);
    ?>
</div>
