<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \common\models\User;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Benutzer verwalten';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Benutzer anlegen', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'last_name',
            'first_name',
            'email',
            [
                'attribute' => 'created_at',
                'format' =>  ['date', 'php:d.m.Y H:i:s'],
            ],
            [
                'attribute' => 'updated_at',
                'format' =>  ['date', 'php:d.m.Y H:i:s'],
            ],
            [
                'attribute' => 'role',
                'content' => function($model){
                    return \common\models\User::$roles[$model->role];
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]) ?>

</div>
