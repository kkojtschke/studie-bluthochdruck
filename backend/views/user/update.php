<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$this->title = $user->first_name . ' ' . $user->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Benutzer verwalten', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['view', 'id' => $user->id]];
$this->params['breadcrumbs'][] = 'Bearbeiten';
?>
<div class="user-update">
    <?php
    echo $this->render('_form', [
        'user' => $user,
    ]);
    ?>

</div>
