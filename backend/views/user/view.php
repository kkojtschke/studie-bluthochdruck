<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$this->title = $user->first_name . ' ' . $user->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Benutzer verwalten', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Bearbeiten', ['update', 'id' => $user->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Löschen', ['delete', 'id' => $user->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Wollen Sie den Benutzer wirklich löschen?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $user,
        'attributes' => [
            'first_name',
            'last_name',
            'email',
            [
                'attribute' => 'role',
                'value' => \common\models\User::$roles[$user->role]
            ],
            [
                'attribute' => 'created_at',
                'format' =>  ['date', 'php:d.m.Y H:i:s'],
            ],
            [
                'attribute' => 'updated_at',
                'format' =>  ['date', 'php:d.m.Y H:i:s'],
            ],
        ],
    ]) ?>

</div>
