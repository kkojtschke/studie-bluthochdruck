/**
 * field_settings.js
 *
 * Diese Datei beinhaltet Funktionen für das Speichern der Feldeinstellungen als JSON.
 */

/**
 * @param index Index des Formularelements.
 */
function initRadioListSettings(index){
    var $csvListItems = $('#csv-list-items-' + index),
        $hiddenInput = $('#field-' + index + '-settings');

    var items = JSON.parse($hiddenInput.val() || '[]').items || [];
    $csvListItems.val(items.toString()
        .replace(/([a-z][^,]*)/ig, '"$1"')
        .replace(/,/g, ', ')
    );

    $csvListItems.change(function(){
        var items = [];
        try{
            items = JSON.parse('[' + this.value + ']');
            $hiddenInput.val(JSON.stringify({
                'items': items
            }));
        }catch(e){
        }
    });
};

/**
 * @param index Index des Formularelements.
 */
function initTableSettings(index){
    var defaultSettings = {'columns': [], 'addText': 'Neues Medikament', 'maxRows': 0},
        $columnEditor = $('#column-editor-' + index),
        $helperInput = $('[name="helper-input-' + index + '"]'),
        $hiddenInput = $('#field-' + index + '-settings'),
        $addText = $('#add-text'),
        $maxRows = $('#max-rows');

    var settings = $.extend({}, defaultSettings, $hiddenInput.val() != '' ? JSON.parse($hiddenInput.val()) : {});
    $hiddenInput.val(JSON.stringify(settings));
    var columns = settings.columns;
    $helperInput.val(JSON.stringify(columns));

    initTableEditor($columnEditor);

    // wenn in der Tabelle etwas geändert wird, dann die Änderung ins hiddenInput zum Speichern übernehmen
    $columnEditor.on('json-updated', function(){
        $.extend(settings, JSON.parse('{"columns":' + $helperInput.val() + '}'));
        $hiddenInput.val(JSON.stringify(settings));
    });

    $addText.change(function(){
        $.extend(settings, {'addText': $addText.val()});
        $hiddenInput.val(JSON.stringify(settings));
    });

    $maxRows.change(function(){
        $.extend(settings, {'maxRows': $maxRows.val()});
        $hiddenInput.val(JSON.stringify(settings));
    });
};

/**
 * @param index Index des Formularelements.
 */
function initHeadlineSettings(index){
    var $headlineLevel = $('#headline-level-' + index),
        $hiddenInput = $('#field-' + index + '-settings');

    var level = JSON.parse($hiddenInput.val() || '[]').level || [];
    $headlineLevel.val(level);

    $headlineLevel.change(function(){
        var level = this.value;
        $hiddenInput.val(JSON.stringify({
            'level': level
        }));
    });
};