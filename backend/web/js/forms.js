/**
 * functions.js
 *
 * Diese Datei beinhaltet Funktionen, die für Formulareingaben benötigt werden.
 */

$(function(){
    initDependentFields();
    initTableEditor();
    initBloodPressureTableEditor();
});

/**
 * Initialisiert die Funktion der abhängigen Felder.
 * Das Attribut data-dependent-on beinhaltet den Namen des Feldes, dessen Wert = 1 sein muss, damit das abhängige Feld
 * angezeigt wird.
 */
function initDependentFields(){
    var triggerFields = [];
    $('[data-dependent-on]').each(function(){
        var dependentOn = $(this).attr('data-dependent-on');
        if(!triggerFields[dependentOn]){
            triggerFields[dependentOn] = [];
        }

        triggerFields[dependentOn].push(this);
    });

    for(var name in triggerFields){
        var selector = '[name="' + name + '"]';

        $(selector).data('fields-to-trigger', $(triggerFields[name]));

        $(selector).change(function(){
            var $elementToToggle = $(this).data('fields-to-trigger').closest('.form-group');
            if($('[name="' + this.name + '"]' + ':checked').prop('value') == 1){
                $elementToToggle.show();
            }else{
                $elementToToggle.hide();
            }
        });

        $(selector).change();
    }
}

/**
 * Initialisiert die Funktion der Tabellen-Editoren. Diese Speichern die Zelleninhalte als JSON in einem Input,
 * dessen Name über data-json-field angegeben wird.
 * @param $el Das Tabellen-Element (optional). Wenn nicht angegeben, dann werden alle Elemente
 * mit der Klasse .table-editor initialisiert.
 */
function initTableEditor($el){
    if(!$el){
        $el = $('.table-editor');
    }

    $el.each(function(){
        var $this = $(this),
            $thead = $this.find('thead'),
            $tbody = $this.find('tbody'),
            $jsonField = $('[name="' + $this.attr('data-json-field') + '"]'),
            maxRows = $this.attr('data-max-rows') || 0;

        var attributes = [];
        $thead.find('th').each(function(n, th){
            attributes.push(th.innerHTML);
        });

        if($jsonField.length > 0){
            var data = [];
            try{
                data = JSON.parse($jsonField.val());
            }catch(e){
            }

            if(data){
                for(var i = 0; i < data.length - 1; ++i){
                    $tbody.children().first().clone().appendTo($tbody);
                }

                $.each(data, function(rowIndex, row){
                    $.each(row, function(colIndex, column){
                        $tbody.children(':nth-child(' + (rowIndex + 1) + ')')
                            .children(':nth-child(' + (colIndex + 1) + ')').find('input').val(column);
                    });
                });
            }
        }

        var bindEvents = function(){
            $this.find('.add').unbind('click').click(function(e){
                e.preventDefault();

                $tbody.children().first().clone()
                    .find('input').val('')
                    .end()
                    .appendTo($tbody);

                // Zeilen begrenzen
                if(maxRows > 0 && $tbody.children().length == maxRows){
                    $(this).hide();
                }

                bindEvents();
            });

            $this.find('.remove').unbind('click').click(function(e){
                e.preventDefault();
                if($tbody.children().length > 1){
                    $(this).closest('tr').remove();
                }else{
                    $(this).closest('tr').find('input').val('');
                }
                updateJson();

                // Hinzufügen-Link wieder anzeigen, wenn maxRows nicht erreicht ist
                if(maxRows > 0 && $tbody.children().length < maxRows){
                    $this.find('.add').show();
                }
            });

            $tbody.find('input').unbind('change').change(function(){
                updateJson();
            });
        };

        var updateJson = function(){
            var data = [];
            $tbody.children().each(function(n, tr){
                var row = [];

                $(tr).find('input').each(function(i, input){
                    row.push(input.value);
                });

                data.push(row);
            });

            $jsonField.val(JSON.stringify(data));

            $this.trigger('json-updated');
        };

        bindEvents();
    });
}

/**
 * Initialisiert die Funktion der Blutdrucktabellen-Editoren. Diese Speichern die Zelleninhalte als JSON in einem Input,
 * dessen Name über data-json-field angegeben wird.
 * @param $el Das Tabellen-Element (optional). Wenn nicht angegeben, dann werden alle Elemente
 * mit der Klasse .table-editor initialisiert.
 */
function initBloodPressureTableEditor($el){
    if(!$el){
        $el = $('.blood-pressure-table-editor');
    }

    $el.each(function(){
        var $this = $(this),
            $thead = $this.find('thead'),
            $tbody = $this.find('tbody'),
            $jsonField = $('[name="' + $this.attr('data-json-field') + '"]');

        var attributes = [];
        $thead.find('th').each(function(n, th){
            attributes.push(th.innerHTML);
        });

        if($jsonField.length > 0){
            var data = [];
            try{
                data = JSON.parse($jsonField.val());
            }catch(e){
            }

            if(data){
                $.each(data, function(rowIndex, row){
                    $.each(row, function(colIndex, column){
                        $tbody.children(':nth-child(' + (rowIndex + 1) + ')')
                            .children(':nth-child(' + (colIndex + 2) + ')').find('input').val(column);
                    });
                });
            }
        }

        $tbody.find('input').unbind('change').change(function(){
            var data = [];
            $tbody.children().each(function(n, tr){
                var row = [];

                $(tr).find('input').each(function(i, input){
                    row.push(input.value);
                });

                data.push(row);
            });

            $jsonField.val(JSON.stringify(data));

            $this.trigger('json-updated');
        });
    });
}