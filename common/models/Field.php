<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "field".
 *
 * @property integer $id
 * @property integer $questionnaire_id
 * @property string $name
 * @property integer $type
 * @property string $settings
 * @property string $label
 * @property string $default_value
 * @property string $condition
 * @property integer $required
 * @property integer $sorting
 * @property integer $exportable
 */
class Field extends \yii\db\ActiveRecord{
    const TYPE_STRING = 0;
    const TYPE_INTEGER = 10;
    const TYPE_YES_NO = 20;
    const TYPE_DATE = 30;
    const TYPE_FILE = 40;
    const TYPE_RADIO_LIST = 50;
    const TYPE_HEADLINE = 60;
    const TYPE_TABLE = 70;
    const TYPE_MEDIUM_TEXT = 80;
    const TYPE_BLOOD_PRESSURE_TABLE = 90;
    public static $typeLabels = [
        self::TYPE_STRING => 'kurzer Text',
        self::TYPE_INTEGER => 'Ganzzahl',
        self::TYPE_YES_NO => 'Ja/Nein',
        self::TYPE_DATE => 'Datum',
        self::TYPE_FILE => 'Datei',
        self::TYPE_RADIO_LIST => 'Einfachauswahl',
        self::TYPE_HEADLINE => 'Überschrift',
        self::TYPE_TABLE => 'Tabelle',
        self::TYPE_MEDIUM_TEXT => 'mittlerer Text',
        self::TYPE_BLOOD_PRESSURE_TABLE => 'Blutdrucktabelle',
    ];

    const REQUIRED_NO = 0;
    const REQUIRED_YES = 1;
    public static $requiredLabels = [
        self::REQUIRED_NO => 'Nein',
        self::REQUIRED_YES => 'Ja'
    ];

    const EXPORTABLE_NO = 0;
    const EXPORTABLE_YES = 1;
    public static $exportableLabels = [
        self::EXPORTABLE_NO => 'Nein',
        self::EXPORTABLE_YES => 'Ja'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName(){
        return 'field';
    }

    /**
     * @inheritdoc
     */
    public function rules(){
        return [
            [['questionnaire_id', 'name'], 'required'],
            [['questionnaire_id', 'type', 'required', 'sorting', 'condition', 'exportable'], 'integer'],
            [['default_value', 'label', 'settings'], 'string'],
            [['default_value'], 'default', 'value' => ''],
            [['type'], 'default', 'value' => self::TYPE_STRING],
            [['exportable'], 'default', 'value' => self::EXPORTABLE_YES],
            [['name', 'label'], 'string', 'max' => 255],
            ['required', 'in', 'range' => array_keys(self::$requiredLabels)],
            ['type', 'in', 'range' => array_keys(self::$typeLabels)]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(){
        return [
            'id' => 'ID',
            'questionnaire_id' => 'Questionnaire ID',
            'name' => 'Name',
            'type' => 'Typ',
            'settings' => 'Einstellungen',
            'label' => 'Beschriftung',
            'default_value' => 'Standardwert',
            'condition' => 'Abhängig von',
            'required' => 'Pflichtfeld',
            'sorting' => 'Sortierung',
            'exportable' => 'Wird exportiert',
        ];
    }
}
