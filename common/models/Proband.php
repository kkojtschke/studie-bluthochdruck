<?php

namespace common\models;

use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "proband".
 *
 * @property integer $id
 * @property integer $sex
 * @property string $street
 * @property integer $street_number
 * @property string $city
 * @property string $zip_code
 * @property string $country
 * @property integer $date_of_birth
 * @property string $title
 * @property string $phone_prefix
 * @property string $phone_number
 * @property string $month_and_year_of_diagnosis
 * @property integer $has_yet_attended_a_treatment
 * @property string $time_since_last_treatment
 * @property integer $count_of_treatments
 * @property string $how_took_notice
 * @property string $upload_einverstaendniserklaerung
 * @property string $upload_eignungsbogen
 * @property int $created_at
 * @property int $updated_at
 * @property int $biosens_id
 * @property string $email
 * @property string $first_name
 * @property string $last_name
 * @property string $kennung
 * @property string $filled_questionnaires
 * @property int $admitted
 */
class Proband extends \yii\db\ActiveRecord{
    /**
     * @inheritdoc
     */
    public static function tableName(){
        return 'proband';
    }

    /**
     * @inheritdoc
     */
    public function rules(){
        return [
            [
                ['sex', 'street', 'street_number', 'city', 'zip_code', 'country', 'date_of_birth', 'phone_prefix',
                    'first_name', 'last_name', 'phone_number'],
                'required'
            ],
            [['biosens_id', 'sex', 'street_number', 'has_yet_attended_a_treatment',
                'count_of_treatments', 'admitted'], 'integer'],
            [['upload_einverstaendniserklaerung', 'upload_eignungsbogen',
                'time_since_last_treatment', 'filled_questionnaires', 'month_and_year_of_diagnosis'],
                'string'],
            [['email'], 'string', 'max' => 255],
            [['first_name', 'last_name', 'street', 'city', 'title', 'country', 'kennung'], 'string', 'max' => 20],
            [['zip_code', 'phone_prefix'], 'string', 'max' => 10],
            [['phone_number'], 'string', 'max' => 15],
            [['how_took_notice'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(){
        return [
            'id' => 'ID',
            'sex' => 'Geschlecht',
            'street' => 'Straße',
            'street_number' => 'Hausnummer',
            'city' => 'Ort',
            'country' => 'Land',
            'zip_code' => 'PLZ',
            'date_of_birth' => 'Geburtstag',
            'title' => 'Titel',
            'phone_prefix' => 'Vorwahl',
            'phone_number' => 'Telefonnummer',
            'month_and_year_of_diagnosis' => 'Wann wurde(n) die Erkrankung bzw. die Beschwerden diagnostiziert'
                . ' (Monat/Jahr)?',
            'has_yet_attended_a_treatment' => 'Hat Dein Proband schon einmal bei einem Biosens an Sitzungen'
                . ' teilgenommen?',
            'count_of_treatments' => 'Wieviele Sitzungen hatte Dein Proband in etwa <em>insgesamt</em>?',
            'how_took_notice' => 'Wie ist Dein Proband auf die BioMeZ-Studie "Biomeditation bei Bluthochdruck"'
                . ' 2015/2016 aufmerksam geworden?',
            'upload_einverstaendniserklaerung' => 'Einverständniserklärung',
            'upload_eignungsbogen' => 'Eignungsbogen',
            'created_at' => 'Registriert am',
            'updated_at' => 'Geändert am',
            'first_name' => 'Vorname',
            'last_name' => 'Nachname',
            'time_since_last_treatment' => 'Zeit seit der letzten Sitzung',
            'amitted' => 'Ist zugelassen?',
        ];
    }

    /**
     * Wird vor dem Speichern aufgerufen.
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert){
        // Datum richtig formatieren
        $this->date_of_birth = strtotime($this->date_of_birth);

        // hochgeladene Dateien speichern
        $oldModel = self::findOne(['id' => $this->id]);
        $uploadFields = [
            'upload_einverstaendniserklaerung', 'upload_eignungsbogen'
        ];

        if(isset($_FILES['Proband'])){
            foreach($uploadFields as $fieldName){
                $tmpNameArray = $_FILES['Proband']['tmp_name'];
                if(isset($tmpNameArray[$fieldName]) && !empty($tmpNameArray[$fieldName])){
                    $this->$fieldName = file_get_contents($tmpNameArray[$fieldName]);

                }else{
                    if($oldModel){
                        $this->$fieldName = $oldModel->$fieldName;
                    }
                }
            }
        }

        return parent::beforeSave($insert);
    }
}
