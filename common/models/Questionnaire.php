<?php

namespace common\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "questionnaire".
 *
 * @property integer $id
 * @property integer $survey_id
 * @property string $title
 * @property integer $group
 * @property string $export_name
 * @property string $hide_when_cancelled
 */
class Questionnaire extends \yii\db\ActiveRecord{
    const GROUP_REGISTRIERUNG = -10;
    const GROUP_ERSTERHEBUNG = 0;
    const GROUP_NACHERHEBUNG = 10;

    public static $groupLabels = [
        self::GROUP_REGISTRIERUNG => 'Registrierung',
        self::GROUP_ERSTERHEBUNG => 'Ersterhebung',
        self::GROUP_NACHERHEBUNG => 'Nacherhebung'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName(){
        return 'questionnaire';
    }

    /**
     * @inheritdoc
     */
    public function rules(){
        return [
            [['survey_id', 'title', 'export_name'], 'required'],
            [['survey_id', 'group', 'hide_when_cancelled'], 'integer'],
            [['title', 'export_name'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(){
        return [
            'id' => 'ID',
            'survey_id' => 'Survey ID',
            'title' => 'Titel',
            'export_name' => 'Name des Tabellenblattes in der Excel-Datei',
            'hide_when_cancelled' => 'Ausblenden, wenn der Proband die Studie abgebrochen hat',
            'group' => 'Gruppe'
        ];
    }

    /**
     * @return Field|null
     */
    public function getFields(){
        return $this->hasMany(Field::className(), ['questionnaire_id' => 'id'])->indexBy('id');
    }
}
