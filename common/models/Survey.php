<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "survey".
 *
 * @property integer $id
 * @property string $title
 * @property integer $start_time
 * @property integer $end_time
 */
class Survey extends \yii\db\ActiveRecord{
    /**
     * @inheritdoc
     */
    public static function tableName(){
        return 'survey';
    }

    /**
     * @inheritdoc
     */
    public function rules(){
        return [
            [['title'], 'required'],
            [['start_time', 'end_time'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(){
        return [
            'id' => 'ID',
            'title' => 'Titel',
            'start_time' => 'Startzeit',
            'end_time' => 'Endzeit',
        ];
    }
}
