<?php

namespace common\models;

use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "value".
 *
 * @property integer $id
 * @property integer $field_id
 * @property integer $proband_id
 * @property mixed $content
 * @property string $additional_data
 */
class Value extends \yii\db\ActiveRecord{
    /**
     * @inheritdoc
     */
    public static function tableName(){
        return 'value';
    }

    /**
     * @inheritdoc
     */
    public function rules(){
        $rules = [
            [['field_id', 'proband_id'], 'required'],
            [['field_id', 'proband_id'], 'integer'],
            [['additional_data'], 'string']
        ];

        switch($this->field->type){
            case Field::TYPE_STRING:
                $rules[] = [['content'], 'string', 'max' => 255,
                    'message' => $this->field->name . ' darf maximal 255 Zeichen lang sein.'];
                break;
            case Field::TYPE_INTEGER:
                $rules[] = [['content'], 'integer', 'message' => $this->field->name . ' muss eine Ganzzahl sein.'];
                break;
            case Field::TYPE_FILE:
                $rules[] = [['content'], 'file'];
                break;
            case Field::TYPE_YES_NO:
                $rules[] = [['content'], 'boolean'];
                break;
            case Field::TYPE_RADIO_LIST:

            case Field::TYPE_TABLE:
            case Field::TYPE_BLOOD_PRESSURE_TABLE:
                $rules[] = [['content'], 'string'];
                break;
            case Field::TYPE_DATE:
                $rules[] = [['content'], 'string'];
                break;
            case Field::TYPE_MEDIUM_TEXT:
                $rules[] = [['content'], 'string', 'max' => 65535,
                    'message' => $this->field->name . ' darf maximal 65.535 Zeichen lang sein.'];
                break;
        }

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(){
        return [
            'id' => 'ID',
            'field_id' => 'Field ID',
            'proband_id' => 'Proband ID',
            'content' => 'Content',
        ];
    }

    public function getField(){
        return $this->hasOne(Field::className(), ['id' => 'field_id']);
    }

    public function getProband(){
        return $this->hasOne(Proband::className(), ['id' => 'proband_id']);
    }

    public function getAdditonalDataAsArray(){
        return Json::decode($this->additional_data);
    }

    /**
     * Wird vor dem Speichern aufgerufen.
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert){
        // wenn der Inhalt geleert wird, dann auch die Zusatzdaten löschen
        if($this->content == null && $this->field->type != Field::TYPE_FILE){
            $this->content = $this->field->default_value;
            $this->additional_data = '{}';
        }

        switch($this->field->type){
            case Field::TYPE_DATE:
                $this->content = strtotime($this->content);
                break;
            case Field::TYPE_FILE:
                if(isset($_FILES['Value'])){
                    $oldModel = self::findOne(['id' => $this->id]);
                    $tmpNameArray = $_FILES['Value']['tmp_name'][$this->field->sorting];

                    if(isset($tmpNameArray['content']) && !empty($tmpNameArray['content'])){
                        $mimeType = $_FILES['Value']['type'][$this->field->sorting]['content'];

                        if(!in_array($mimeType, ['application/pdf', 'image/jpeg'])){
                            return false;
                        }

                        $this->content = file_get_contents($tmpNameArray['content']);

                        $this->additional_data = Json::encode([
                            'fileName' => $_FILES['Value']['name'][$this->field->sorting]['content'],
                            'mimeType' => $mimeType
                        ]);
                    }else{
                        if($oldModel){
                            $this->content = $oldModel->content;
                        }
                    }
                }
                break;
            default:
                $this->additional_data = '{}';
                break;
        }

        return parent::beforeSave($insert);
    }
}
